package starter.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {
    @JsonProperty("provider")
    public String provider;
    @JsonProperty("title")
    public String title;
    @JsonProperty("url")
    public String url;
    @JsonProperty("brand")
    public String brand;
    @JsonProperty("price")
    public String price;
    @JsonProperty("unit")
    public String unit;
    @JsonProperty("isPromo")
    public String isPromo;
    @JsonProperty("promoDetails")
    public String promoDetails;
    @JsonProperty("image")
    public String image;


    public String getProvider() {
        return provider;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getBrand() {
        return brand;
    }

    public String getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getIsPromo() {
        return isPromo;
    }

    public String getPromoDetails() {
        return promoDetails;
    }

    public String getImage() {
        return image;
    }
}
