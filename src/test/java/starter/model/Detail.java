package starter.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Detail {
    @JsonProperty("error")
    private boolean error;

    @JsonProperty("message")
    private String message;

    @JsonProperty("requested_item")
    private String requestedItem;

    @JsonProperty("served_by")
    private String servedBy;

    public void setError(boolean error) {
        this.error = error;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setRequestedItem(String requestedItem) {
        this.requestedItem = requestedItem;
    }

    public void setServedBy(String servedBy) {
        this.servedBy = servedBy;
    }

    public boolean getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public String getRequestedItem() {
        return requestedItem;
    }

    public String getServedBy() {
        return servedBy;
    }
}
