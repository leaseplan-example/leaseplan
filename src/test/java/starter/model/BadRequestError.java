package starter.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BadRequestError {
    @JsonProperty("detail")
    private Detail detail;

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public Detail getDetail() {
        return detail;
    }
}