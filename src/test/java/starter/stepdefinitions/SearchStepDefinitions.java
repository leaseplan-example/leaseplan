package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.http.HttpStatus.SC_OK;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.assertj.core.api.Assertions.assertThat;

import static starter.data.TestData.BASE_URL;
import static starter.data.TestData.generateErrorResponse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import starter.model.BadRequestError;
import starter.model.Product;

public class SearchStepDefinitions {
    private static final Logger logger = LoggerFactory.getLogger(SearchStepDefinitions.class);

    private Response response;

    @When("user calls endpoint to search product: \"([^\"]*)\"$")
    public void heCallsEndpoint(String productName) {
        response = SerenityRest.given()
                .baseUri(BASE_URL)
                .when()
                .get(productName);
    }

    @Then("user sees the results displayed for product: \"([^\"]*)\"$")
    public void everyProductOfResponseShouldContainSearchParameter(String searchParam) {
        List<Product> productList = response.then()
                .statusCode(SC_OK)
                .extract()
                .body()
                .jsonPath()
                .getList(".", Product.class);

        logger.info("List of products: ");
        logger.info(String.valueOf(productList));

        productList.forEach(product -> assertThat(product.getTitle()).containsIgnoringCase(searchParam));
    }

    @Then("user sees the results displayed for searching parameter: \"([^\"]*)\"$")
    public void everyTitleOfResponseShouldContainSearchParam(String searchParam) {
        List<String> titleList = response.then()
                .statusCode(SC_OK)
                .extract()
                .body()
                .jsonPath()
                .getList(".", Product.class)
                .stream()
                .map(Product::getTitle)
                .collect(Collectors.toList());

        logger.info("List of titles: ");
        logger.info(String.valueOf(titleList));

        assertThat(titleList, everyItem(containsStringIgnoringCase(searchParam)));
    }

    @Then("user sees the results displayed for search: \"([^\"]*)\"$")
    public void everyProductOfResponseShouldContainSearchParam(String searchParam) {
        ArrayList responseItemsList = response.then()
                .statusCode(SC_OK)
                .log()
                .body()
                .extract()
                .body()
                .as(ArrayList.class);

        logger.info("List of response items: ");
        logger.info(String.valueOf(responseItemsList));

        responseItemsList.forEach(product -> assertThat(product.toString()).containsIgnoringCase(searchParam));
    }

    @Then("user does not see the results {string}")
    public void userDoesNotSeeTheResults(String searchParam) {
        BadRequestError actualRequestResponse = response.then()
                .statusCode(SC_NOT_FOUND)
                .extract()
                .response()
                .then()
                .extract()
                .as(BadRequestError.class);

        BadRequestError expectedResult = generateErrorResponse(searchParam);

        assertThat(actualRequestResponse.getDetail()).isEqualToComparingFieldByField(expectedResult.getDetail());
    }
}
