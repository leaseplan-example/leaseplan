package starter.data;

import starter.model.BadRequestError;
import starter.model.Detail;

public class TestData {
    public static BadRequestError generateErrorResponse(String searchParam) {
        BadRequestError expectedResult = new BadRequestError();
        Detail detail = new Detail();
        detail.setError(true);
        detail.setMessage("Not found");
        detail.setRequestedItem(searchParam);
        detail.setServedBy("https://waarkoop.com");
        expectedResult.setDetail(detail);
        return expectedResult;
    }

    public static final String BASE_URL = "https://waarkoop-server.herokuapp.com/api/v1/search/demo/";

}
