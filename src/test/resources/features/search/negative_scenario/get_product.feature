Feature: Search for the product negative scenarios

#### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
#### Available products: "orange", "apple", "pasta", "cola"
#### Prepare Positive and negative scenarios

  Scenario Outline: Search for product
    When user calls endpoint to search product: "<searchParam>"
    Then user does not see the results "<searchParam>"

    Examples:
      | searchParam |
      | bike        |
      | bus         |
      | Car         |