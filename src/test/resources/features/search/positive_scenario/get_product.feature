Feature: Search for the product positive scenarios

#### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
#### Available products: "orange", "apple", "pasta", "cola"
#### Prepare Positive and negative scenarios

 # TODO fails because some fields 'title' doesn't contain 'orange', 'pasta'
  Scenario Outline: Search for product
    When user calls endpoint to search product: "<searchParam>"
    Then user sees the results displayed for product: "<searchParam>"

    Examples:
      | searchParam |
      | apple       |
      | cola        |
      | orange      |
      | pasta       |
